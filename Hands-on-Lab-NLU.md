<img src="images/IDSNlogo.png" width="200" height="200"/>


## Hands-On Lab: NLU(Natural Language Understanding)(20 Mins)

#### Objective


- Learn about **NLU**.
- Learn about **GAAMA** which is a **Natural Language Understanding** technology created by IBM Researchers.


## NLU

**Natural-language understanding** (NLU) is a subtopic of **natural-language processing** in **artificial intelligence** which has the ability to process text and understand its meaning. NLU directly enables human-computer interaction

One of its interesting applications is **Question Answering** which you will see in this demo.  

## GAAMA

IBM Research creates innovative tools and resources to help unleash the power of AI.

**GAAMA** is basically a search engine for individual documents. When you submit a question, GAAMA uses natural language capabilities to extract specific answers from the document at hand, including scores that indicate how confident the system is in each answer.


#### Follow these steps to explore the demo:


1. Access the Demo here: [GAAMA: An answer engine for your documents](https://reading-comprehension-website.mybluemix.net/)

2. Select one topic, by default **Current Events** is selected. Now, please read the Article.

   <img src="images/1.PNG"  width="600">
 
   Note: Every time you reload the page you will see the different content.



3. Ask a question related to the document. Sample questions are given for your help, you can also choose one of them.

   <img src="images/2.PNG"  width="600">

   As you can see in the below screenshot, **GAAMA** uses natural language capabilities to extract specific answers from the 
   document including it's confidence scores. The answer with the highest confidence score is always on the top.

   <img src="images/3.PNG"  width="600">

4. Now select the **Wikipedia** topic, read the article and ask questions, check whether or not you are getting the 
   correct answers.

    <img src="images/4.png"  width="600">

5. Explore the other topics(Contracts,Technote).

#### Optional 

You can use your custom text and try. For that **click** on the **Custom** option and write or paste your text.


 <img src="images/4.png"  width="600">

 ### Congratulations on completing this demo!

 ## Author(s)

 [Shubham Yadav](https://www.linkedin.com/in/shubham-kumar-yadav-14378768)

 ## Changelog
| Date | Version | Changed by | Change Description |
|------|--------|--------|---------|
| 17-11-2020 | 3.0 | Shubham | Created Demo instructions |
|   |   |   |   |
